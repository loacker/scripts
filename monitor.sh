#!/bin/bash

LAPTOP="eDP-1"
HDMI="HDMI-1"
MON_POS="/tmp/monitor_position"

[[ ! -e $MON_POS ]] &&  (echo "off" | tee $MON_POS)

if [ "$(cat $MON_POS)" == "off" ];then
    `which xrandr` --output $HDMI --auto --same-as $LAPTOP --auto
    echo "same-as" | tee $MON_POS > /dev/null 2>&1
elif [ "$(cat $MON_POS)" == "same-as" ];then
    `which xrandr` --output $HDMI --auto --above $LAPTOP --auto
    echo "above" | tee $MON_POS > /dev/null 2>&1
elif [ "$(cat $MON_POS)" == "above" ];then
    `which xrandr` --output $HDMI --off
    echo "off" | tee $MON_POS > /dev/null 2>&1
fi


# vim:set ts=4 sw=4 ft=sh et:
