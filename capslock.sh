#!/bin/bash

while [[ $(xset -q | awk '/Caps/ {print $4}') == "on" ]]; do
    `which notify-send` -u critical "CapsLock On"
done

`which notify-send` -u critical "CapsLock Off" -t 300

