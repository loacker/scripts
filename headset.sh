#!/usr/bin/env bash

RFKILL_HCI=$(rfkill list bluetooth --output TYPE,DEVICE,SOFT --noheadings | awk '/hci0/ {print $3}')

if [[ $(systemctl is-active bluetooth) == "active" ]]; then
    while [[ -h /sys/class/bluetooth/hci0 ]]; do
        if [[ -n $RFKILL_HCI ]]; then
            DEVICE=$(bluetoothctl devices | awk '{print $2}')
            bluetoothctl connect $DEVICE
            [[ $? == 0 ]] && break
        fi
    done
    if [[ -n $DEVICE ]]; then
        SINK_NAME=$(pactl list cards short | awk '/bluez/ {print $2}')
        pactl set-card-profile $SINK_NAME headset_head_unit
    fi
    if [[ -n $(prep -f mpd) ]]; then
        systemctl --user restart mpd
    fi
fi

# vim: set ts=8 sw=8 sts=0 tw=0 ff=unix ft=sh et ai :
